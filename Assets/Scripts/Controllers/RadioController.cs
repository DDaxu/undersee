﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioController : MonoBehaviour
{

    public AudioSource MusicSource;
    public AudioClip MusicClip;
    public AudioClip MusicClip2;
    public AudioClip MusicClip3;
    public AudioClip MusicClip4;
    public PlayerObject player;


    void OnMouseOver()
    {
        if(Input.GetKeyDown("e"))
        {
            var list = new List<AudioClip>{MusicClip, MusicClip2, MusicClip3, MusicClip4};
            var random = new System.Random();
            int index = random.Next(list.Count);
            var randomMusic = list[index];
            MusicSource.clip = randomMusic;
            MusicSource.Play();
        }
    }
}


