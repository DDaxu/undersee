﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class characterController : MonoBehaviour
{
    public float speed = 10.0f;
    public bool onGround = true;
    public float jumpForce = 200f;
    // Start is called before the first frame update
    void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float translation = Input.GetAxis("Vertical") * speed;
        float straffe = Input.GetAxis("Horizontal") * speed;
        translation *= Time.deltaTime;
        straffe *= Time.deltaTime;

        transform.Translate(straffe, 0 , translation);
        if (Input.GetKeyDown("escape"))
            Cursor.lockState = CursorLockMode.None;

        RaycastHit hit;
        Vector3 physicsCentre = this.transform.position +
                                this.GetComponent<CapsuleCollider>().center;

        if (Physics.Raycast(physicsCentre, Vector3.down, out hit, 0.8f))
        {
            if (hit.transform.gameObject.tag != "Player")
            {
                onGround = true;
            }
        }
        else
        {
            onGround = false;
        }
        if (Input.GetKeyDown("space") && onGround != false)
        {
            this.GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce);
        }

    }
}
