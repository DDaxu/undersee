﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuController : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenuUI;
    [SerializeField] private bool isPaused;
    public FirstPersonAIO AIO;
   public TerminalController TC;
    private void Update()
    {
        if(TC.panelActive == false){
            //Debug.Log("läläläläläläläläl");
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;
        if (isPaused)
        {

            ActivateMenu();

        } else
        {
            DeactivateMenu();

        }
        }
        }
    }
    void ActivateMenu()
    {
       isPaused = true;
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.None; 
        Cursor.visible = true;
        //AIO.PauseMenu(isPaused);
        pauseMenuUI.SetActive(true);
    }

    //Deactivating menu with button does not work because CameraController is hardcoded.
    public void DeactivateMenu()
    {
        Time.timeScale = 1;
        pauseMenuUI.SetActive(false);
        isPaused = false;
        Cursor.visible = false;
        //AIO.PauseMenu(isPaused);
        Cursor.lockState = CursorLockMode.Locked; 
        Cursor.visible = false;

    }
    public void ExitApplication()
    {
        Application.Quit();
    }
}
