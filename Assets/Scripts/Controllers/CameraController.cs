﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController: MonoBehaviour
{
    Vector2 mouseLook;
    Vector2 smoothV;
    public float sensitivity = 2.0f;
    public float smoothing = 2.0f;

	// Variables for camera zoom
	int zoomFOV = 20;
	int normalFOV = 60;
	float smooth = 5;

	private bool isZoomed = false;
    public bool paused = true;


    GameObject Player;
    // Start is called before the first frame update
    void Start()
    {

        Player = this.transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
            if (!paused) {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
                md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
                smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smoothing);
                smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smoothing);
                mouseLook += smoothV;
                mouseLook.y = Mathf.Clamp(mouseLook.y, -60, 60);

                transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
                Player.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, Player.transform.up);

		        //Camera Zoom
		        if (Input.GetMouseButtonDown(1))
                {
                    isZoomed = !isZoomed;
                }
		
                if (isZoomed)
                {
                    // Incrementally zoom to marked FOV state
                    GetComponent<Camera>().fieldOfView = Mathf.Lerp(GetComponent<Camera>().fieldOfView, zoomFOV, Time.deltaTime * smooth);
                }
                else
                {
                    // Incrementally zoom out to normal FOV state
                    GetComponent<Camera>().fieldOfView = Mathf.Lerp(GetComponent<Camera>().fieldOfView, normalFOV, Time.deltaTime * smooth);
                }
        }
    }
}
