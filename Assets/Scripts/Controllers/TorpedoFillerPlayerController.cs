﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TorpedoFillerPlayerController : MonoBehaviour
{

    public PlayerObject player;
    public SubmarineObject submarine;
    [SerializeField] public RawImage Torpedo;

    void OnMouseOver()
    {
        if(submarine.TorpedoesCapacity() == false){
            if(Input.GetKeyDown("e"))
            {
                player.addTorpedo();
                Torpedo.enabled = true;
            }
        }
    }
}
