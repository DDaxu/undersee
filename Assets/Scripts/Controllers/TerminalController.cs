﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerminalController : MonoBehaviour
{
    [SerializeField] private GameObject terminalPanelUI;
    [SerializeField] public bool panelActive;

      public FirstPersonAIO AIO;
void OnMouseOver()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            panelActive = !panelActive;

     if (panelActive)
        {
            ActivateTerminal();
        }
        else
        {
            DeactivateTerminal();
        }
        }

    }
    void ActivateTerminal()
    {
        panelActive = true;
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.None; 
        Cursor.visible = true;
        terminalPanelUI.SetActive(true);
    }

    //Deactivating menu with button does not work because CameraController is hardcoded.
    void DeactivateTerminal()
    {
        Time.timeScale = 1;
        terminalPanelUI.SetActive(false);
        panelActive = false;
        Cursor.lockState = CursorLockMode.Locked; 
        Cursor.visible = false;

    }

    public void Moi(){
        Debug.Log("moi");
    }
}
