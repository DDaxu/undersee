﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientSoundController : MonoBehaviour
{

    public AudioClip AmbientSoundClip;
    public AudioSource MusicSource;
    public float time = 60f;

    void Start(){
        MusicSource.clip = AmbientSoundClip;
    }

    void Update()
    {
        if (time > 0) {
            time -= Time.deltaTime;
        }
        
        else {
            MusicSource.Play();
            time = 600f;
        }
    }
}