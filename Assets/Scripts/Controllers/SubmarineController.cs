﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubmarineController : MonoBehaviour
{
    // Start is called before the first frame update
    public SubmarineObject submarine;

    // Update is called once per frame
    void Update()
    {
        //temporal, these are for testing.
        if(Input.GetKeyDown(KeyCode.F1))
        {
            submarine.addTorpedo();
        }
        if(Input.GetKeyDown(KeyCode.F2))
        {
            submarine.removeTorpedo();
        }
        if(Input.GetKeyDown(KeyCode.F3))
        {
            submarine.addHealth();
        }
        if(Input.GetKeyDown(KeyCode.F4))
        {
            submarine.removeHealth();
        }
    }
}
