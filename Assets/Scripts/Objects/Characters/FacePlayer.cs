﻿using UnityEngine;
using System.Collections;
 
public class FacePlayer : MonoBehaviour
{
    //public Camera m_Camera;
    public GameObject target;
 
    //Orient the camera after all movement is completed this frame to avoid jittering
    void LateUpdate()
    {
        /*
        
        transform.forward = Camera.main.transform.forward;
        transform.rotation = Quaternion.Euler(0, 90, 0);
        */
        Vector3 targetPosition = new Vector3(target.transform.position.x,transform.position.y,target.transform.position.z);
        transform.LookAt(targetPosition);
    }
}
