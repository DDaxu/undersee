﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerObject : MonoBehaviour
{
    public GameObject text;
    public int Torpedo = 0;
    public bool hasTorpedoes = false;
    public void addTorpedo(){

        if(Torpedo < 1)
        {
            
            Torpedo++;
            Debug.Log(Torpedo);
        }
    }
    public void removeTorpedo(){
        if(Torpedo > 0){
            Torpedo--;
            Debug.Log(Torpedo);
        }
    }
    public bool Torpedoes(){
        if(Torpedo > 0){
            hasTorpedoes = true;
            return hasTorpedoes;
        }else{
            hasTorpedoes = false;
            return hasTorpedoes;
        }
    }
    void Update(){
  text.GetComponent<TextMesh>().text = Torpedo.ToString();
    }
}

