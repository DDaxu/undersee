﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("e")) {
        Vector3 bar = transform.position;
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.AddComponent<Rigidbody>();
            cube.AddComponent<MeshFilter>();
            cube.AddComponent<BoxCollider>();
            cube.transform.position = bar;
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {
            Vector3 bar = transform.position;
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.AddComponent<Rigidbody>();
            cube.AddComponent<MeshFilter>();
            cube.AddComponent<BoxCollider>();
            cube.transform.position = bar;
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {
            Vector3 bar = transform.position;
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.AddComponent<Rigidbody>();
            cube.AddComponent<MeshFilter>();
            cube.AddComponent<BoxCollider>();
            cube.transform.position = bar;
        }
    }
}
